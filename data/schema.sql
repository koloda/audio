CREATE SCHEMA IF NOT EXIST `audio` DEFAULT CHARACTER SET utf8 ;

CREATE TABLE IF NOT EXIST `audio`.`album` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `artist` VARCHAR(255) NOT NULL,
  `year` INT(4) UNSIGNED NOT NULL,
  `duration` INT(3) UNSIGNED NOT NULL,
  `boughtAt` DATE NOT NULL,
  `price` FLOAT UNSIGNED NOT NULL,
  `storageCode` VARCHAR(45) NOT NULL,
  `cover` VARCHAR(255) NULL ,
  PRIMARY KEY (`id`),
  INDEX `titleindex` (`title` ASC),
  INDEX `artistindex` (`artist` ASC),
  INDEX `yearindex` (`year` ASC),
  INDEX `storageindex` (`storageCode` ASC));

INSERT INTO `audio`.`album` (`title`, `artist`, `year`, `duration`, `boughtAt`, `price`, `storageCode`) VALUES ('Where Did You Sleep Last Night: Lead Belly Legacy, Vol. 1', 'Lead Belly', '1960', '56', '1989-01-01', '30.50', '12:323:45');
INSERT INTO `audio`.`album` (`title`, `artist`, `year`, `duration`, `boughtAt`, `price`, `storageCode`) VALUES ('King of the Delta Blues Singers', 'Robert Johnson', '1965', '72', '1993-07-12', '47.00', '12:11:76');

