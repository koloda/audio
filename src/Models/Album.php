<?php

namespace Audio\Models;

use Audio\Helpers\Session;

class Album
{
    public $validationErrors = [];

    private $db;

    private $filters = [
        'id'        => FILTER_SANITIZE_NUMBER_INT,
        'title'     => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
        'artist'    => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
        'year'      => [
            'filter'    => FILTER_VALIDATE_INT,
            'options'   => ['min_range' => 1800]
        ],
        'duration'  => FILTER_SANITIZE_NUMBER_INT,
        'boughtAt'  => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
        'price'     => FILTER_SANITIZE_NUMBER_FLOAT,
        'st0'       => FILTER_SANITIZE_NUMBER_INT,
        'st1'       => FILTER_SANITIZE_NUMBER_INT,
        'st2'       => FILTER_SANITIZE_NUMBER_INT,
        'cover'     => FILTER_SANITIZE_URL,
    ];

    private $sortings = [
        'title', 'title DESC',
        'artist', 'artist DESC',
        'year', 'year DESC',
        'price', 'price DESC',
        'boughtAt', 'boughtAt DESC',
    ];

    public function __construct(\PDO $db)
    {
        $this->db = $db;
        $this->filters['year']['options']['max_range'] = date('Y');
    }

    public function create()
    {
        $fields = $this->validateInput(['cover', 'id']);

        if (count($this->validationErrors)) {
            //bad idea to use sessions in models
            //Session::set('errors', $this->validationErrors);
            //Session::set('input', $fields);
            return false;
        }

        $statement = $this->db->prepare(
            'INSERT INTO `album` (`title`, `artist`, `year`, `duration`, `boughtAt`, `price`, `storageCode`, `cover`)
            VALUES (:t, :a, :y, :d, :b, :p, :s, :c)'
        );

        if ($statement->execute(
            [
                ':t'    => $fields['title'],
                ':a'    => $fields['artist'],
                ':y'    => $fields['year'],
                ':d'    => $fields['duration'],
                ':b'    => $fields['boughtAt'],
                ':p'    => (float) $fields['price'],
                ':s'    => implode(':', [$fields['st0'], $fields['st1'], $fields['st2']]),
                ':c'    => $fields['cover']
            ]
        )) {
            //and again
            //Session::unset('input');
            return true;
        }

        return false;
    }

    public function update(int $id)
    {
        $item = static::find($id);

        if (!$item) {
            return false;
        }
        
        $fields = $this->validateInput(['cover']);
        
        if (count($this->validationErrors)) {
            //and again
            //Session::set('errors', $this->validationErrors);
            //Session::set('input', $fields);
            return false;
        }

        $statement = $this->db->prepare(
            'UPDATE `album` 
            SET `title`=:t, `artist`=:a, `year`=:y, `duration`=:d, `boughtAt`=:b, `price`=:p, `storageCode`=:s, `cover`=:c
            WHERE `id` = :id'
        );

        if ($statement->execute(
            [
                ':t'    => $fields['title'],
                ':a'    => $fields['artist'],
                ':y'    => $fields['year'],
                ':d'    => $fields['duration'],
                ':b'    => $fields['boughtAt'],
                ':p'    => (float) $fields['price'],
                ':s'    => implode(':', [$fields['st0'], $fields['st1'], $fields['st2']]),
                ':c'    => $fields['cover'],
                ':id'   => $fields['id']
            ]
        )) {
            //and again
            //Session::unset('input');
            return true;
        }

        return false;
    }

    public function getList(string $order = null, string $artist = null)
    {
        $sql = 'SELECT `id`, `title`, `artist`, `year`, `cover`, `duration`, `boughtAt`, `price`, `storageCode` 
            FROM `album`';

        if ($artist) {
            $sql .= ' WHERE `artist` LIKE \'' . $artist .'\'';
        }

        if ($order && in_array($order, $this->sortings)) {
            $sql .= " ORDER BY {$order}";
        }

        return $this->db->query($sql)
            ->fetchAll();
    }

    public function find(int $id)
    {
        $statement = $this->db->prepare(
            'SELECT `id`, `title`, `artist`, `year`, `cover`, `duration`, `boughtAt`, `price`, `storageCode` 
            FROM `album`
            WHERE `id` = :id'
        );

        if ($statement->execute([':id' => $id])) {
            $item = $statement->fetch();

            if ($item) {
                $parts = explode(':', $item['storageCode']);
                $item['st0'] = $parts[0];
                $item['st1'] = $parts[1];
                $item['st2'] = $parts[2];

                return $item;
            }
        }

        return null;
    }

    public function getArtists()
    {
        return $this->db->query('SELECT DISTINCT(`artist`) FROM `album`')->fetchAll();
    }

    private function validateInput(array $skip = ['cover'])
    {
        Session::unset('errors');
        $fields = filter_input_array(INPUT_POST, $this->filters);
        $this->validationErrors = [];

        foreach ($this->filters as $key => $v) {
            if (!in_array($key, $skip) && !$fields[$key]) {
                $fields[$key] = trim($fields[$key]);

                if ($fields[$key] === false) {
                    $this->validationErrors[$key] = 'Bad field format';
                } else {
                    $this->validationErrors[$key] = 'Field is required';
                }
            }
        }

        return $fields;
    }
}
