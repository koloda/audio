<?php

namespace Audio\Components;

abstract class BaseRequest
{
    protected $queryParams = [];
    protected $postData = [];

    public function __construct(string $url)
    {
        parse_str(parse_url($url, PHP_URL_QUERY), $this->queryParams);
        $this->postData = $_POST;
    }

    public function handle()
    {}

    public function render(array $data, string $tpl)
    {
        $tplPath = realpath(__DIR__ .'/../templates/'. $tpl . '.php');
        $layoutPath = realpath(__DIR__ .'/../templates/_layout.php');

        if (file_exists($tplPath)) {
            include_once($layoutPath);
        }
    }
}