<?php

namespace Audio;

use Audio\Components\BaseRequest;

class App
{
    private static $request;
    private static $db;
    private static $config;

    public static function init(BaseRequest $request)
    {
        static::$request = $request;
        session_start();
    }

    public static function run()
    {
        try {
            echo static::$request->handle();
        } catch (\ErrorException $e) {
            http_response_code($e->getCode());
            exit($e->getCode() .' : '. $e->getMessage());
        }
    }

    public static function getDb(): \PDO
    {
        if (!static::$db) {
            try {
                $config = static::getConfig();
                static::$db = new \PDO(
                    $config['db']['dsn'],
                    $config['db']['user'],
                    $config['db']['pass']
                );
            } catch (\Exception $e) {
                throw new \ErrorException('Cannot connect to database');
            }
        }

        return static::$db;
    }

    public static function getConfig()
    {
        if (!static::$config) {
            static::$config = require realpath(__DIR__ . '/../config/main.php');
        }

        return static::$config;
    }
}
