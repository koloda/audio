<?php

require 'autoload.php';

error_reporting(E_ALL);
ini_set('display_errors', 'on');

$defaultRoute = 'list';
$url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
$path = str_replace('/', '', parse_url($url, PHP_URL_PATH)) ?: $defaultRoute;
$className = 'Audio\\Requests\\' . ucfirst($path) . 'Request';

try {
    $request = new $className($url);
} catch (\Error $e) {
    http_response_code(404);
    echo '404: Not found';
}

return $request;