<?php
/** @var array $items */
?>

<div class="row">

    <div class="col-sm-12">
        <form class="form-inline" action="/list" method="GET">
            <div class="row">
                <div class="col-md-8">
                <label for="" class="form-label">Исполнитель: </label>
                    <select name="artist" class="form-control" style="width: 100%;">
                        <option value="">Все</option>
                        <?php foreach($artists as $a): ?>
                        
                            <option value="<?=$a['artist']?>"  <?php if($artist == $a['artist']): ?> selected="selected" <?php endif; ?> ><?=$a['artist']?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="col-md-4 text-center">
                    <label for="" class="form-label">Сортировка: </label>
                    <select name="sort" class="form-control">
                        <option value=""></option>
<option value="title" <?php if($sort=='title'): ?> selected="selected" <?php endif; ?>  >Название</option>
                        <option <?php if($sort=='title DESC'): ?> selected="selected" <?php endif; ?>  value="title DESC">Название ↓</option>
                
                        <option <?php if($sort=='artist'): ?> selected="selected" <?php endif; ?> value="artist">Исполнитель</option>
                        <option <?php if($sort=='artist DESC'): ?> selected="selected" <?php endif; ?> value="artist DESC">Исполнитель ↓</option>
        
                        <option <?php if($sort=='year'): ?> selected="selected" <?php endif; ?> value="year">Год</option>
                        <option <?php if($sort=='year DESC'): ?> selected="selected" <?php endif; ?> value="year DESC">Год ↓</option>
        
                        <option <?php if($sort=='price'): ?> selected="selected" <?php endif; ?> value="price">Цена</option>
                        <option <?php if($sort=='price DESC'): ?> selected="selected" <?php endif; ?> value="price DESC">Цена ↓</option>
        
                        <option <?php if($sort=='boughtAt'): ?> selected="selected" <?php endif; ?> value="boughtAt">Куплено</option>
                        <option <?php if($sort=='boughtAt DESC'): ?> selected="selected" <?php endif; ?> value="boughtAt DESC">Куплено ↓</option>
                
                    </select>
                    <input type="submit" value="Ok" class="btn btn-light">
                </div>
            </div>
        </form>
        <hr>

    </div>
    
</div>

<div class="row">
    <?php foreach($items as $item): ?>
    
    <div class="col-md-6">
        <div class="card">
            <img src="<?=$item['cover']?>" alt="<?=$item['artist'] .' - '. $item['title']?>" class="card-img-top">

            <div class="card-body">
                <h5 class="card-title"><?=$item['title']?></h5>
                <p class="card-text">
                    <i>Автор:</i>
                    <?=$item['artist']?>
                </p>
            </div>

            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    Год релиза: <b><?=$item['year']?></b>
                </li>
                <li class="list-group-item">
                    Длительность (мин): <b><?=$item['duration']?></b>
                </li>
                <li class="list-group-item">
                    Куплен: <b><?=$item['boughtAt']?></b>
                </li>
                <li class="list-group-item">
                    Цена: <b>$<?=round($item['price'], 2)?></b>
                </li>
                <li class="list-group-item">
                    Код хранилища: <?=$item['storageCode']?>
                </li>
                <li class="list-group-item text-right">
                    <a href="/edit?id=<?=$item['id']?>" class="btn btn-sm btn-danger">
                    Редактировать
                    </a>
                </li>
            </ul>
        </div>
    </div>
    
    <?php endforeach; ?>
    
</div>

<a href="/create" id="add-album-btn" class="btn btn-danger">
    +
</a>