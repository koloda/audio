<?php

use Audio\Helpers\Session;
use Audio\Helpers\Html;

/** @var array $fields */
?>

<div class="card">
    <div class="card-header">
        <!-- <h3> -->
            <?=$head?>
        <!-- </h3> -->
    </div>
    <div class="card-body">

        <form action="<?=$action?>" method="post">

        <?php if ($id): ?>
        <input type="hidden" name="id" value="<?=$id?>">
        <?php endif; ?>

        <div class="form-group row">
            <label for="" class="col-sm-3 col-form-label">Название альбома</label>
            <div class="col-sm-9">
                <input value="<?=Html::fieldValue('title')?>" name="title" type="text" class="form-control" maxlength="255" required placeholder="Best blues songs of 60's">
                <?=Html::fieldError('title')?>
            </div>
        </div>

        <div class="form-group row">
            <label for="" class="col-sm-3 col-form-label">Исполнитель</label>
            <div class="col-sm-9">
                <input value="<?=Html::fieldValue('artist')?>" name="artist" type="text" class="form-control" maxlength="255" placeholder="SomeBand">
                <?=Html::fieldError('artist')?>
            </div>
        </div>

        <div class="form-group row">
            <label for="" class="col-sm-3 col-form-label">Год релиза</label>
            <div class="col-sm-9">
                <input value="<?=Html::fieldValue('year')?>" name="year" type="number" class="form-control" min="1800" max="<?=date('Y')?>" required step="1" placeholder="1976">
                <?=Html::fieldError('year')?>
            </div>
        </div>

        <div class="form-group row">
            <label for="" class="col-sm-3 col-form-label">Длительность (мин)</label>
            <div class="col-sm-9">
                <input value="<?=Html::fieldValue('duration')?>" name="duration" type="number" class="form-control" required min="1" max="999" step="1" placeholder="60">
                <?=Html::fieldError('duration')?>
            </div>
        </div>

        <div class="form-group row">
            <label for="" class="col-sm-3 col-form-label">Куплен</label>
            <div class="col-sm-9">
                <input value="<?=Html::fieldValue('boughtAt')?>" name="boughtAt" type="date" class="form-control" required>
                <?=Html::fieldError('boughtAt')?>
            </div>
        </div>

        <div class="form-group row">
            <label for="" class="col-sm-3 col-form-label">Цена</label>
            <div class="col-sm-9">
                <input value="<?=Html::fieldValue('price')?>" name="price" type="number" class="form-control" min="0" step="0.01" placeholder="10.99">
                <?=Html::fieldError('price')?>
            </div>
        </div>

        <div class="form-group row">
            <label for="" class="col-sm-3 col-form-label">Код хранилища</label>
            <div class="col-sm-9">
                <div class="row">
                    <div class="col">
                        <input value="<?=Html::fieldValue('st0')?>" name="st0" type="number" class="form-control" min="0" step="1" placeholder="31" max="999">
                        <?=Html::fieldError('st0')?>
                    </div>
                    <div class="col">
                        <input value="<?=Html::fieldValue('st1')?>" name="st1" type="number" class="form-control" min="0" step="1" placeholder="22" max="999">
                        <?=Html::fieldError('st1')?>
                    </div>
                    <div class="col">
                        <input value="<?=Html::fieldValue('st2')?>" name="st2" type="number" class="form-control" min="0" step="1" placeholder="78" max="999">
                        <?=Html::fieldError('st2')?>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label for="" class="col-sm-3 col-form-label">Обложка <small><i>(URL, необязательно)</i></small></label>
            <div class="col-sm-9">
                <input value="<?=Html::fieldValue('cover')?>" name="cover" type="url" class="form-control" placeholder="https://images.com/somepic.png">
                <?=Html::fieldError('cover')?>
            </div>
        </div>

        <hr>
        <div class="form-group text-right">
            <input type="submit" class="btn btn-success" value="Сохранить">
        </div>
        </form>
    </div>
</div>