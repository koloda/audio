<?php

namespace Audio\Helpers;

class Html
{
    private static $errors;
    private static $input;

    public static function fieldError(string $name)
    {
        $errors = static::getErrors();

        if (isset($errors[$name])) {
            echo "<div class=\"invalid-feedback\">{$errors[$name]}</div>";
        }
    }

    public static function fieldValue($name)
    {
        $input = static::getInput();

        return isset($input[$name]) ? $input[$name] : '';
    }

    public static function getInput()
    {
        if (!static::$input) {
            static::$input = Session::get('input', []);
        }

        return static::$input;
    }

    public static function getErrors()
    {
        if (!static::$errors) {
            static::$errors = Session::get('errors', []);
        }

        return static::$errors;
    }

    public static function bindModel(array $fields)
    {
        static::$input = $fields;
    }
}