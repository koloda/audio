<?php

namespace Audio\Helpers;

class Session
{
    public static function set(string $key, $value)
    {
        $_SESSION[$key] = $value;
    }

    public static function get(string $key, $default = null)
    {
        return isset($_SESSION[$key]) ? $_SESSION[$key] : $default;
    }

    public static function unset(string $key)
    {
        unset($_SESSION[$key]);
    }
}