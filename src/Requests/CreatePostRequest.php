<?php

namespace Audio\Requests;

use Audio\Components\BaseRequest;
use Audio\Models\Album;
use Audio\App;
use Audio\Helpers\Url;

class CreatePostRequest extends BaseRequest
{
    private $repo;

    public function __construct(string $url)
    {
        parent::__construct($url);

        $this->repo = new Album(App::getDb());
    }

    public function handle()
    {
        if ($this->repo->create()) {
            Url::redirect('/list');
        } else {
            Url::redirect('/create');
        }
    }
}