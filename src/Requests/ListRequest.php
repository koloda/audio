<?php

namespace Audio\Requests;

use Audio\Models\Album;
use Audio\App;

class ListRequest extends \Audio\Components\BaseRequest
{
    private $repo;
    private $sort;
    private $artist;

    public function __construct(string $url)
    {
        parent::__construct($url);

        $this->repo = new Album(App::getDb());
        $this->sort = $this->queryParams['sort'] ?? null;
        $this->artist = $this->queryParams['artist'] ?? null;
    }

    public function handle()
    {
        $items = $this->repo->getList($this->sort, $this->artist);

        return $this->render(['items' => $items, 'sort' => $this->sort, 'artists' => $this->repo->getArtists(), 'artist' => $this->artist], 'list');
    }
}