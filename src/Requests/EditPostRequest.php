<?php

namespace Audio\Requests;

use Audio\Components\BaseRequest;
use Audio\Models\Album;
use Audio\App;
use Audio\Helpers\Url;

class EditPostRequest extends BaseRequest
{
    private $repo;

    public function __construct(string $url)
    {
        parent::__construct($url);

        $this->repo = new Album(App::getDb());
    }

    public function handle()
    {

        if (!$this->postData['id']) {
            throw new \ErrorException('Bad request', 400);
        }

        $id = (int) $this->postData['id'];

        if ($this->repo->update($id)) {
            Url::redirect('/list');
        } else {
            Url::redirect('/edit?id=' . $id);
        }
    }
}