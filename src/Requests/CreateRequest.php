<?php

namespace Audio\Requests;

use Audio\Components\BaseRequest;

class CreateRequest extends BaseRequest
{
    public function __construct(string $url)
    {
        parent::__construct($url);
    }

    public function handle()
    {
        return $this->render(
            [
                'action'    => '/createPost', 
                'id'        => false,
                'head'      => 'Добавление альбома',
            ], 
            'form'
        );
    }
}