<?php

namespace Audio\Requests;

use Audio\Components\BaseRequest;
use Audio\Models\Album;
use Audio\App;
use Audio\Helpers\Session;
use Audio\Helpers\Html;

class EditRequest extends BaseRequest
{
    private $repo;

    public function __construct(string $url)
    {
        parent::__construct($url);

        $this->repo = new Album(App::getDb());
    }

    public function handle()
    {
        if (!isset($this->queryParams['id'])) {
            throw new \ErrorException('Bad request', 400);
        }

        $item = $this->repo->find((int) $this->queryParams['id']);
        
        if (!$item) {
            throw new \ErrorException('Not found', 404);
        }

        Html::bindModel($item);
        
        return $this->render(
            [
                'id'    => $item['id'],
                'head'  => 'Редактирование албома <b>' . $item['title'] . '</b>',
                'action'    => '/editPost'
            ], 
            'form'
        );
    }
}